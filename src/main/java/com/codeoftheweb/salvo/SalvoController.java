package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping("/api")
public class SalvoController {

    //Cada mapping realizado se muestra a continuación y posteriormente sus maps y lists correspondientes según sea el caso
    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private  ScoreRepository scoreRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    // MAPPINGs --------------------------------------------------------------------------------------------------------

    @RequestMapping(path = "/players", method = RequestMethod.POST)
    private ResponseEntity<Object> register(@RequestParam String username, @RequestParam String password) {

        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>("Missing data", HttpStatus.FORBIDDEN);
        }

        Player player = playerRepository.findByUserName(username);
        if (player !=  null) {
            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
        }
        if (username.length()>30)
            return new ResponseEntity<>(makeMap("error", "Excedio el maximo de caracteres"), HttpStatus.FORBIDDEN);

        playerRepository.save(new Player(username, passwordEncoder.encode(password)));
        return new ResponseEntity<>("Name added", HttpStatus.CREATED);
    }

    @RequestMapping("/games")
    private Map<String, Object> makeLoggedPlayer(Authentication authentication){
        Map<String, Object> dto = new LinkedHashMap<>();
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null)
            dto.put("player", "Guest");
        else
            dto.put("player", makePlayerDTO(authenticatedPlayer));
        dto.put("games", getGames());

        return dto;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    private ResponseEntity<Map<String, Object>> createGame(Authentication authentication) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null){
            return new ResponseEntity<>(makeMap("error","No name given"), HttpStatus.FORBIDDEN);
        } else {
            Date date = Date.from(java.time.ZonedDateTime.now().toInstant());
            Game auxGame = new Game(date);
            gameRepository.save(auxGame);

            GamePlayer auxGameP = new GamePlayer(authenticatedPlayer,auxGame);
            gamePlayerRepository.save(auxGameP);

            return new ResponseEntity<>(makeMap("gpid", auxGameP.getId()), HttpStatus.CREATED);
        }
    }

    @RequestMapping(path = "/game/{IdGame}/players", method = RequestMethod.POST)
    private ResponseEntity<Map<String, Object>> joinGame(@PathVariable Long IdGame,Authentication authentication) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player authenticatedPlayer = getAuthentication(authentication);
        if(authenticatedPlayer == null){
            return new ResponseEntity<>(makeMap("error","No estas logueado"), HttpStatus.FORBIDDEN);
        } else {

            Game gameAux = gameRepository.findById(IdGame).get();
            int cantidadDeGPPorJuego = gameAux.getGamePlayers().stream().collect(toSet()).size();

            if(gameAux == null) {
                return new ResponseEntity<>(makeMap("error", "No hay tal juego"), HttpStatus.FORBIDDEN);
            }
            if(cantidadDeGPPorJuego > 1){
                return new ResponseEntity<>(makeMap("error","El juego está lleno"), HttpStatus.FORBIDDEN);
            }
            GamePlayer auxGameP = new GamePlayer(authenticatedPlayer,gameAux);
            gamePlayerRepository.save(auxGameP);
            return new ResponseEntity<>(makeMap("gpid", auxGameP.getId()), HttpStatus.CREATED);

        }
    }

    @RequestMapping(value = "/games/players/{id}/ships", method = RequestMethod.POST)
    private ResponseEntity<Map<String,Object>> AddShips(@PathVariable long id,@RequestBody List<Ship> ships,Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository.findById(id).orElse(null);
        Player loggedPlayer = getAuthentication(authentication);
        if (loggedPlayer == null)
            return new ResponseEntity<>(makeMap("error", "no player logged in"), HttpStatus.UNAUTHORIZED);
        if (gamePlayer == null)
            return new ResponseEntity<>(makeMap("error", "no such gamePlayer"), HttpStatus.UNAUTHORIZED);
        if (wrongGamePlayer(gamePlayer, loggedPlayer)) {
            return new ResponseEntity<>(makeMap("error", "Wrong GamePlayer"), HttpStatus.UNAUTHORIZED);
        } else {
            if (gamePlayer.getShips().isEmpty()) {
                ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
                gamePlayer.setShips(ships);
                shipRepository.saveAll(ships);
<<<<<<< HEAD
                return new ResponseEntity<>(makeMap("ok", "Ships saved"), HttpStatus.CREATED);
=======
                return new ResponseEntity<>(makeMap("OK", "Barcos Ubicados"), HttpStatus.CREATED);
>>>>>>> 2c5a525... "projecto finalizado"
            } else {
                return new ResponseEntity<>(makeMap("error", "Player already has ships"), HttpStatus.FORBIDDEN);
            }
        }
    }

    @RequestMapping(value = "/games/players/{id}/salvoes", method = RequestMethod.POST)
    private ResponseEntity<Map<String,Object>> AddSalvoes ( @PathVariable long id,@RequestBody Salvo salvos,Authentication authentication) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Player loggedPlayer = getAuthentication(authentication);
        if (loggedPlayer == null) {
            return new ResponseEntity<>(makeMap("error", "no player logged in"), HttpStatus.UNAUTHORIZED);
        }
        GamePlayer gamePlayer = gamePlayerRepository.findById(id).get();
        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "no such gamePlayer"), HttpStatus.UNAUTHORIZED);
        }
        if (wrongGamePlayer(gamePlayer, loggedPlayer)) {
            return new ResponseEntity<>(makeMap("error", "wrong GamePlayer"), HttpStatus.UNAUTHORIZED);
        } else {
            if(gamePlayerConSalvos(gamePlayer.getSalvoes(), salvos)){
                return new ResponseEntity<>(makeMap("error","Player already has salvos"), HttpStatus.FORBIDDEN);
            } else {
                salvos.setGamePlayer(gamePlayer);
                salvoRepository.save(salvos);
<<<<<<< HEAD
                return new ResponseEntity<>(makeMap("ok","Salvo saved"), HttpStatus.CREATED);
=======
                return new ResponseEntity<>(makeMap("OK","Salvos Lanzados"), HttpStatus.CREATED);
>>>>>>> 2c5a525... "projecto finalizado"
            }
        }
    }

    @RequestMapping(path ="/game_view/{id}" )
    public ResponseEntity<Map<String,Object>> AuthorizeUser(@PathVariable long id, Authentication authentication){

        Map<String, Object> dto = new LinkedHashMap<>();
        Player loggedPlayer = getAuthentication(authentication);
        if (loggedPlayer == null) {
            return new ResponseEntity<>(makeMap("error","no player logged in"), HttpStatus.FORBIDDEN);
        }
        GamePlayer gamePlayer = gamePlayerRepository.getOne(id);
        if(wrongGamePlayer(gamePlayer,loggedPlayer)){
            return new ResponseEntity<>(makeMap("error","Unauthorized"), HttpStatus.UNAUTHORIZED);
        }
        if(id > numberGamePlayers()){
            return new ResponseEntity<>(makeMap("error","no such GamePlayer"), HttpStatus.FORBIDDEN);
        }

        GamePlayer opponent = GetOpponent(gamePlayer.getGame(),gamePlayer).orElse(null);

        if(opponent == null){
            return new ResponseEntity<>(makeMap("error","waiting for next player"),HttpStatus.FORBIDDEN);
        }

        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gameState", getGameState(gamePlayer,opponent));
        dto.put("gamePlayers", getGamePlayerList(gamePlayer.getGame().getGamePlayers()));
        if (gamePlayer.getShips().isEmpty()) {
            dto.put("ships", new ArrayList<>());
        }else{
            dto.put("ships", getShipList(gamePlayer.getShips()));
        }
        if(opponent == null || gamePlayer.getSalvoes().isEmpty()) {
            dto.put("salvoes", new ArrayList<>());
        }else {
            dto.put("salvoes", getSalvoList(gamePlayer.getGame()));
        }
        if (opponent.getShips().isEmpty() || opponent.getSalvoes().isEmpty()) {
            dto.put("hits", EmptyHits());
        }else {
            dto.put("hits",makeHitsDTO(gamePlayer,opponent));
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping("/leaderBoard")
    private List<Map<String,Object>> makeLeaderBoard(){
        return playerRepository
                .findAll()
                .stream()
                .map(player -> makeLeaderBoardDTO(player))
                .collect(toList());

    }


    // DTOs ------------------------------------------------------------------------------------------------------------

    private Map<String, Object> makePlayerDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("username", player.getUserName());
        dto.put ("score", getScoreList(player));
        return dto;
    }

    private Map<String,Object> Player2DTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", player.getId());
        dto.put("userName", player.getUserName());
        return dto;
    }

    private Map<String, Object> makeGameDTO(Game game) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", game.getId());
        dto.put("created", game.getCreationDate());
        dto.put("gamePlayers", getGamePlayerList(game.getGamePlayers()));
        dto.put("scores", makeGamesScoresList(game.getScores()));
        return dto;
    }

    private Map<String, Object> makeGamePlayerDTO(GamePlayer gamePlayer) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("gpid", gamePlayer.getId());
        dto.put("joinDate", gamePlayer.getJoinDate());
        dto.put("player", makePlayerDTO(gamePlayer.getPlayer()));
        return dto;
    }

    private Map<String,Object> makeShipDTO(Ship ship){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("shipType", ship.getShipType());
        dto.put("locations", ship.getShipLocations());
        return dto;
    }

    private Map<String,Object> makeSalvoDTO(Salvo salvo){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("turn", salvo.getTurn());
        dto.put("player", salvo.getGamePlayer().getPlayer().getId());
        dto.put("locations", salvo.getSalvoLocations());
        return dto;
    }

    private Map<String, Object> makeScoreDTO(Score score) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("playerID", score.getPlayer().getId());
        dto.put("score", score.getScore());
        dto.put("finishDate", score.getFinishDate());
        return dto;
    }

    private Map<String, Object> makeLeaderBoardDTO(Player player) {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", player.getId());
        dto.put("name", player.getUserName());
        dto.put("scores", getScoreList(player));
        return dto;
    }

    private Map<String,Object> makeHitsDTO(GamePlayer self, GamePlayer opponent){
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("self", getHits(self,opponent));
        dto.put("opponent", getHits(opponent,self));
        return dto;
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    private String getGameState(GamePlayer self, GamePlayer opponent) {
        List<Ship> selfShips = self.getShips();
        List<Salvo> selfSalvoes = self.getSalvoes();

        if (selfShips.size() == 0){
            return "PLACESHIPS";
        }
        if (opponent.getShips() == null){
            return "WAITINGFOROPP";
        }
        long turn = getCurrentTurn(self, opponent);
        List<Ship> opponentShips = opponent.getShips();
        List<Salvo> opponentSalvoes = opponent.getSalvoes();
        if (opponentShips.size() == 0){
            return "WAIT";
        }
        if(selfSalvoes.size() == opponentSalvoes.size()){
            Player selfPlayer = self.getPlayer();
            Game game = self.getGame();
            if (allPlayerShipsSunk(selfShips, opponentSalvoes) && allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(selfPlayer, game, 0.5f, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "TIE";
            }
            if (allPlayerShipsSunk(selfShips, opponentSalvoes)){
                Score score = new Score(selfPlayer, game, 0, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "LOST";
            }
            if(allPlayerShipsSunk(opponentShips, selfSalvoes)){
                Score score = new Score(selfPlayer, game, 1, new Date());
                if(!existScore(score, game)) {
                    scoreRepository.save(score);
                }
                return "WON";
            }
        }
        if (selfSalvoes.size() != turn){
            return "PLAY";
        }
        return "WAIT";
    }


    // LISTAS ----------------------------------------------------------------------------------------------------------

    private List<Object> getGames() {
        { return gameRepository
                .findAll()
                .stream()
                .map(game -> makeGameDTO(game))
                .collect(toList());
        }
    }

    private List<Map<String, Object>> getGamePlayerList(List<GamePlayer> gamePlayers) {
        { return gamePlayers
                .stream()
                .map(gamePlayer -> makeGamePlayerDTO(gamePlayer))
                .collect(toList());
        }
    }

    private List<Map<String, Object>> getShipList(List<Ship> ships) {
        { return ships
                .stream()
                .map(ship -> makeShipDTO(ship))
                .collect(toList());
        }
    }

    private List<Map<String, Object>> makeSalvoList(List<Salvo> salvoes) {
        return salvoes
                .stream()
                .map(salvo -> makeSalvoDTO(salvo))
                .collect(toList());
    }

    private List<Object> makeGamesScoresList(Set<Score> scores) {
        return scores
                .stream()
                .map(score -> makeScoreDTO(score))
                .collect(toList());
    }

    private List<Map<String,Object>> getSalvoList(Game game){
        List<Map<String,Object>> myList = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> myList.addAll(makeSalvoList(gamePlayer.getSalvoes())));
        return myList;
    }

    private Map<String, Object> getScoreList (Player player) {
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("name", player.getUserName());
        dto.put("total", player.getScore(player));
        dto.put("won", player.getWins(player.getScores()));
        dto.put("tied", player.getDraws(player.getScores()));
        dto.put("lost", player.getLoses(player.getScores()));
        return dto;
    }

    private List<Map> getHits(GamePlayer self, GamePlayer opponent){
        List<Map> dto = new ArrayList<>();
        int carrierDamage = 0;
        int destroyerDamage = 0;
        int patrolboatDamage = 0;
        int submarineDamage = 0;
        int battleshipDamage = 0;
        List<String> carrierLocations = new ArrayList<>();
        List<String> destroyerLocations = new ArrayList<>();
        List<String> submarineLocations = new ArrayList<>();
        List<String> patrolboatLocations = new ArrayList<>();
        List<String> battleshipLocations = new ArrayList<>();
        for (Ship ship: self.getShips()) {
            switch (ship.getShipType()){
                case "carrier":
                    carrierLocations = ship.getShipLocations();
                    break ;
                case "battleship" :
                    battleshipLocations = ship.getShipLocations();
                    break;
                case "destroyer":
                    destroyerLocations = ship.getShipLocations();
                    break;
                case "submarine":
                    submarineLocations = ship.getShipLocations();
                    break;
                case "patrolboat":
                    patrolboatLocations = ship.getShipLocations();
                    break;
            }
        }
        for (Salvo salvo : opponent.getSalvoes()) {
            Integer carrierHitsInTurn = 0;
            Integer battleshipHitsInTurn = 0;
            Integer submarineHitsInTurn = 0;
            Integer destroyerHitsInTurn = 0;
            Integer patrolboatHitsInTurn = 0;
            Integer missedShots = salvo.getSalvoLocations().size();
            Map<String, Object> hitsMapPerTurn = new LinkedHashMap<>();
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();
            List<String> salvoLocationsList = new ArrayList<>();
            List<String> hitCellsList = new ArrayList<>();
            salvoLocationsList.addAll(salvo.getSalvoLocations());
            for (String salvoShot : salvoLocationsList) {
                if (carrierLocations.contains(salvoShot)) {
                    carrierDamage++;
                    carrierHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (battleshipLocations.contains(salvoShot)) {
                    battleshipDamage++;
                    battleshipHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (submarineLocations.contains(salvoShot)) {
                    submarineDamage++;
                    submarineHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (destroyerLocations.contains(salvoShot)) {
                    destroyerDamage++;
                    destroyerHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
                if (patrolboatLocations.contains(salvoShot)) {
                    patrolboatDamage++;
                    patrolboatHitsInTurn++;
                    hitCellsList.add(salvoShot);
                    missedShots--;
                }
            }
            damagesPerTurn.put("carrierHits", carrierHitsInTurn);
            damagesPerTurn.put("battleshipHits", battleshipHitsInTurn);
            damagesPerTurn.put("submarineHits", submarineHitsInTurn);
            damagesPerTurn.put("destroyerHits", destroyerHitsInTurn);
            damagesPerTurn.put("patrolboatHits", patrolboatHitsInTurn);
            damagesPerTurn.put("carrier", carrierDamage);
            damagesPerTurn.put("battleship", battleshipDamage);
            damagesPerTurn.put("submarine", submarineDamage);
            damagesPerTurn.put("destroyer", destroyerDamage);
            damagesPerTurn.put("patrolboat", patrolboatDamage);
            hitsMapPerTurn.put("turn", salvo.getTurn());
            hitsMapPerTurn.put("hitLocations", hitCellsList);
            hitsMapPerTurn.put("damages", damagesPerTurn);
            hitsMapPerTurn.put("missed", missedShots);
            dto.add(hitsMapPerTurn);
        }
        return dto;
    }

    private Optional<GamePlayer> GetOpponent(Game game, GamePlayer gamePlayer){
        return game.getGamePlayers()
                .stream()
                .filter(opponent -> gamePlayer.getId() != opponent.getId())
                .findFirst();
    }

    // CONDICIONES -------------------------------------------------------------------------------------------------

    private Player getAuthentication (Authentication authentication){
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken){
            return null;
        }else{
            return (playerRepository.findByUserName(authentication.getName()));
        }
    }

    private boolean wrongGamePlayer(GamePlayer gamePlayer, Player player){

        boolean corretGP= gamePlayer.getPlayer().getId() != player.getId();
        return corretGP;
    }

    private boolean gamePlayerConSalvos(List<Salvo> gamePlayerAux, Salvo salvoes){
        boolean tieneSalvo = false;
          for (Salvo s:gamePlayerAux){
            if(s.getTurn() == salvoes.getTurn()){
                tieneSalvo = true;
            }
        }
        return tieneSalvo;
    }

    private int numberGamePlayers (){
        int i = 0;
        for(GamePlayer gamePlayer: gamePlayerRepository.findAll()){
            i++;
        }
        return i;
    }

    private Map<String,Object> makeErrorMap(String message){
        Map<String,Object> msgMap = new LinkedHashMap<>();
        msgMap.put("Error :",message);
        return msgMap;
    }

    private Map<String,Object> EmptyHits(){
        Map<String,Object> dto = new LinkedHashMap<>();
        dto.put("self", new ArrayList<>());
        dto.put("opponent", new ArrayList<>());
        return dto;
    }

    private long getCurrentTurn(GamePlayer self, GamePlayer opponent){
        int selfGPSalvoes = self.getSalvoes().size();
        int opponentGPSalvoes = opponent.getSalvoes().size();

        int totalSalvoes = selfGPSalvoes + opponentGPSalvoes;

        if(totalSalvoes % 2 == 0)
            return totalSalvoes / 2 + 1;

        return (int) (totalSalvoes / 2.0 + 0.5);
    }

    private Boolean allPlayerShipsSunk(List<Ship> selfShips,List<Salvo> oppSalvoes ){
        Map<String,Object> damages = getDamages(selfShips, oppSalvoes);



        long selfSunkenShips = selfShips
                .stream()
                .filter(ship -> Long.parseLong(String.valueOf(damages.get(ship.getShipType()))) == ship.getShipLength(ship))
                .count();

        return selfSunkenShips == 5;

    }

    private  Boolean existScore(Score score, Game game){
        Set<Score> scores = game.getScores();
        for(Score s : scores){
            if(score.getPlayer().getUserName().equals(s.getPlayer().getUserName())){
                return true;
            }
        }
        return false;
    }

    private Map<String,Object> getDamages(List<Ship> selfShip,List<Salvo> oppSalvoes) {
        Map<String, Object> dto = new LinkedHashMap<>();

        int carrierDamage = 0;
        int destroyerDamage = 0;
        int patrolboatDamage = 0;
        int submarineDamage = 0;
        int battleshipDamage = 0;
        List<String> carrierLocations = new ArrayList<>();
        List<String> destroyerLocations = new ArrayList<>();
        List<String> submarineLocations = new ArrayList<>();
        List<String> patrolboatLocations = new ArrayList<>();
        List<String> battleshipLocations = new ArrayList<>();

        for (Ship ship : selfShip) {
            switch (ship.getShipType()) {
                case "carrier":
                    carrierLocations = ship.getShipLocations();
                    break;
                case "battleship":
                    battleshipLocations = ship.getShipLocations();
                    break;
                case "destroyer":
                    destroyerLocations = ship.getShipLocations();
                    break;
                case "submarine":
                    submarineLocations = ship.getShipLocations();
                    break;
                case "patrolboat":
                    patrolboatLocations = ship.getShipLocations();
                    break;
            }
        }


        for (Salvo salvo : oppSalvoes) {
            Map<String, Object> damagesPerTurn = new LinkedHashMap<>();
            List<String> salvoShot = new ArrayList<>();
            salvoShot.addAll(salvo.getSalvoLocations());

            for (String salvoLocation : salvoShot) {
                if (carrierLocations.contains(salvoLocation)) {
                    carrierDamage++;
                }
                if (battleshipLocations.contains(salvoLocation)) {
                    battleshipDamage++;
                }
                if (submarineLocations.contains(salvoLocation)) {
                    submarineDamage++;
                }
                if (destroyerLocations.contains(salvoLocation)) {
                    destroyerDamage++;
                }
                if (patrolboatLocations.contains(salvoLocation)) {
                    patrolboatDamage++;
                }
            }

        }
        dto.put("carrier", carrierDamage);
        dto.put("battleship", battleshipDamage);
        dto.put("submarine", submarineDamage);
        dto.put("destroyer", destroyerDamage);
        dto.put("patrolboat", patrolboatDamage);
        return dto;
    }


}