package com.codeoftheweb.salvo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Ship {

    // ATRIBUTOS -------------------------------------------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String shipType;

    @ElementCollection
    @Column(name="shipLocations")
    private List<String> shipLocations;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gamePlayer_id")
    private GamePlayer gamePlayer;

    // METODOS----------------------------------------------------------------------------------------------------------
    public Ship() { }

    public Ship(String shipType, List<String> shipLocations, GamePlayer gamePlayer) {
        this.setShipType(shipType);
        this.setShipLocations(shipLocations);
        this.setGamePlayer(gamePlayer);
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public List<String> getShipLocations() {
        return shipLocations;
    }

    public void setShipLocations(List<String> shipLocations) {
        this.shipLocations = shipLocations;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }

    public void setGamePlayer(GamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    public long getShipLength(Ship ship){

     long length = 0;
     switch (ship.getShipType()) {
         case "carrier":
             length = 5;
             break;
         case "battleship":
             length = 4;
             break;
         case "submarine":
             length = 3;
             break;
         case "destroyer":
             length = 3;
             break;
         case "patrolboat":
             length = 2;
             break;
     }
      return length;


     }




}




