package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SalvoApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SalvoApplication.class, args);
	}

	@Bean
	public CommandLineRunner initData(PlayerRepository playerRepository,
									  GameRepository gameRepository,
									  GamePlayerRepository gamePlayerRepository,
									  ShipRepository shipRepository,
									  SalvoRepository salvoRepository,
									  ScoreRepository scoreRepository) {
		return (args) -> {

			// PLAYERS -------------------------------------------------------------------------------------------------

			Player p1 = new Player("j.bauer@ctu.gov", passwordEncoder().encode("24"));
			Player p2 = new Player("c.obrian@ctu.gov", passwordEncoder().encode("42"));
			Player p3 = new Player("kim_bauer@gmail.com", passwordEncoder().encode("kb"));
			Player p4 = new Player("t.almeida@ctu.gov", passwordEncoder().encode("mole"));

			playerRepository.save(p1);
			playerRepository.save(p2);
			playerRepository.save(p3);
			playerRepository.save(p4);

			// GAMES ---------------------------------------------------------------------------------------------------

			Date creationDate = new Date();
			Date newDate = Date.from(creationDate.toInstant().plusSeconds(3600));
			Date newDate2 = Date.from(newDate.toInstant().plusSeconds(3600));
			Game g1 = new Game(creationDate);
			Game g2 = new Game(newDate);
			Game g3 = new Game(newDate2);

			gameRepository.save(g1);
			gameRepository.save(g2);
			gameRepository.save(g3);

			// GAMEPLAYERS (4PLAYERS/ 3GAMES)---------------------------------------------------------------------------

			GamePlayer gp1 = new GamePlayer(p1, g1);
			GamePlayer gp2 = new GamePlayer(p2, g1);
			GamePlayer gp3 = new GamePlayer(p1, g2);
			GamePlayer gp4 = new GamePlayer(p2, g2);
			GamePlayer gp5 = new GamePlayer(p3, g3);
			GamePlayer gp6 = new GamePlayer(p4, g3);

			gamePlayerRepository.save(gp1);
			gamePlayerRepository.save(gp2);
			gamePlayerRepository.save(gp3);
			gamePlayerRepository.save(gp4);
			gamePlayerRepository.save(gp5);
			gamePlayerRepository.save(gp6);

			// SHIPS ---------------------------------------------------------------------------------------------------

			//CARRIER
			List<String> shipLocations1 = new ArrayList<>();
			{
				shipLocations1.add("J1");
				shipLocations1.add("J2");
				shipLocations1.add("J3");
				shipLocations1.add("J4");
				shipLocations1.add("J5");
			}

			//BATTLESHIP
			List<String> shipLocations2 = new ArrayList<>();
			{
				shipLocations2.add("A10");
				shipLocations2.add("B10");
				shipLocations2.add("C10");
				shipLocations2.add("D10");
			}

			//SUBMARINE
			List<String> shipLocations3 = new ArrayList<>();
			{
				shipLocations3.add("E10");
				shipLocations3.add("F10");
				shipLocations3.add("G10");
			}

			//DESTROYER
			List<String> shipLocations4 = new ArrayList<>();
			{
				shipLocations4.add("H10");
				shipLocations4.add("I10");
				shipLocations4.add("J10");
			}

			//PATROL BOAT
			List<String> shipLocations5 = new ArrayList<>();
			{
				shipLocations5.add("A1");
				shipLocations5.add("B1");
			}

			String shipType1 = "carrier";//length = 5
			String shipType2 = "battleship"; //length = 4
			String shipType3 = "submarine"; //length = 3
			String shipType4 = "destroyer"; //length = 3
			String shipType5 = "patrolboat"; //length = 2

			Ship ship1 = new Ship(shipType1, shipLocations1, gp1);
			Ship ship2 = new Ship(shipType2, shipLocations2, gp1);
			Ship ship3 = new Ship(shipType3, shipLocations3, gp1);
			Ship ship4 = new Ship(shipType4, shipLocations4, gp1);
			Ship ship5 = new Ship(shipType5, shipLocations5, gp1);
			Ship ship6 = new Ship(shipType1, shipLocations1, gp2);
			Ship ship7 = new Ship(shipType2, shipLocations2, gp2);
			Ship ship8 = new Ship(shipType3, shipLocations3, gp2);
			Ship ship9 = new Ship(shipType4, shipLocations4, gp2);
			Ship ship10 = new Ship(shipType5, shipLocations5, gp2);


			shipRepository.save(ship1);
			shipRepository.save(ship2);
			shipRepository.save(ship3);
			shipRepository.save(ship4);
			shipRepository.save(ship5);
			shipRepository.save(ship6);
			shipRepository.save(ship7);
			shipRepository.save(ship8);
			shipRepository.save(ship9);
			shipRepository.save(ship10);

			// SALVOES -------------------------------------------------------------------------------------------------

			//self - turn1
			List<String> salvoLocations1 = new ArrayList<>();
			{
				salvoLocations1.add("A1");
				salvoLocations1.add("B1");
				salvoLocations1.add("A10");
				salvoLocations1.add("B10");
				salvoLocations1.add("C10");
			}

			//self - turn 2
			List<String> salvoLocations2 = new ArrayList<>();
			{
				salvoLocations2.add("D10");
				salvoLocations2.add("E10");
				salvoLocations2.add("F10");
				salvoLocations2.add("G10");
				salvoLocations2.add("H10");
			}

			//SELF -TURN 3
			List<String> salvoLocations3 = new ArrayList<>();
			{
				salvoLocations3.add("G5");
				salvoLocations3.add("F5");
				salvoLocations3.add("A5");
				salvoLocations3.add("B5");
				salvoLocations3.add("C5");
			}

			//SELF - TURN 4
			List<String> salvoLocations4 = new ArrayList<>();
			{
				salvoLocations4.add("J1");
				salvoLocations4.add("J2");
				salvoLocations4.add("J3");
				salvoLocations4.add("J4");
				salvoLocations4.add("J5");
			}

			// OPPONENT - TURN 1
			List<String> salvoLocations5 = new ArrayList<>();
			{
				salvoLocations5.add("J3");
				salvoLocations5.add("J2");
				salvoLocations5.add("J1");
				salvoLocations5.add("J4");
				salvoLocations5.add("J5");
			}

			// OPPONENT - TURN 2
			List<String> salvoLocations6 = new ArrayList<>();
			{
				salvoLocations6.add("H2");
				salvoLocations6.add("H1");
				salvoLocations6.add("I2");
				salvoLocations6.add("I3");
				salvoLocations6.add("I1");
			}

			// OPPONENT - TURN 3
			List<String> salvoLocations7 = new ArrayList<>();
			{
				salvoLocations7.add("G8");
				salvoLocations7.add("C7");
				salvoLocations7.add("G2");
				salvoLocations7.add("G3");
				salvoLocations7.add("G1");
			}

			// OPPONENT - TURN 4
			List<String> salvoLocations8 = new ArrayList<>();
			{
				salvoLocations8.add("F4");
				salvoLocations8.add("F3");
				salvoLocations8.add("F2");
				salvoLocations8.add("F1");
				salvoLocations8.add("C3");
			}

			Salvo salvo1 = new Salvo(gp1, 1, salvoLocations1);
			Salvo salvo2 = new Salvo(gp1, 2, salvoLocations2);
			Salvo salvo3 = new Salvo(gp1, 3, salvoLocations3);
			Salvo salvo4 = new Salvo(gp1, 4, salvoLocations4);
			Salvo salvo5 = new Salvo(gp2, 1, salvoLocations5);
			Salvo salvo6 = new Salvo(gp2, 2, salvoLocations6);
			Salvo salvo7 = new Salvo(gp2, 3, salvoLocations7);
			Salvo salvo8 = new Salvo(gp2, 4, salvoLocations8);

			salvoRepository.save(salvo1);
			salvoRepository.save(salvo2);
			salvoRepository.save(salvo3);
			salvoRepository.save(salvo4);
			salvoRepository.save(salvo5);
			salvoRepository.save(salvo6);
			salvoRepository.save(salvo7);
			salvoRepository.save(salvo8);

			// SCORES --------------------------------------------------------------------------------------------------

			Date newFinishDate = Date.from(creationDate.toInstant().plusSeconds(1800));
			Date newFinishDate2 = Date.from(newDate2.toInstant().plusSeconds(1800));
			float win = 1;
			float lose = 0;
			float tie = (float) 0.5;

			Score score1 = new Score(p1, g1, win, newFinishDate);
			Score score2 = new Score(p2, g1, lose, newFinishDate);
			Score score3 = new Score(p1, g2, tie, newFinishDate2);
			Score score4 = new Score(p2, g2, tie, newFinishDate2);

			scoreRepository.save(score1);
			scoreRepository.save(score2);
			scoreRepository.save(score3);
			scoreRepository.save(score4);
		};
	}

	// ENCRIPTADOR DE CONTRASEÑA ---------------------------------------------------------------------------------------
	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	PlayerRepository playerRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(inputName -> {
			Player player = playerRepository.findByUserName(inputName);
			if (player != null) {
				return new User(player.getUserName(), player.getPassword(),
						AuthorityUtils.createAuthorityList("USER"));
			} else {
				throw new UsernameNotFoundException("Unknown user: " + inputName);
			}
		});

	}
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin()
				.usernameParameter("username")
				.passwordParameter("password")
				.loginPage("/api/login");

		http.authorizeRequests()
				.antMatchers("/web/games_3.html").permitAll()
				.antMatchers("/web/**").permitAll()
				.antMatchers("/api/games").permitAll()
				.antMatchers("/api/players").permitAll()
				.antMatchers("/api/game_view/**").hasAuthority("USER")
				.antMatchers("/rest").denyAll()
				.anyRequest().permitAll();

		http.logout().logoutUrl("/api/logout")
				.permitAll();

		http.headers().frameOptions().sameOrigin();

		// turn off checking for CSRF tokens
		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		}
	}
}



